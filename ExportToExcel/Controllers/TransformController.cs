﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using ExportToExcel.ExcelWriter;

namespace ExportToExcel.Controllers
{
	public class TransformController : ApiController
	{

		// POST api/transform
		public HttpResponseMessage Post()
		{
			HttpPostedFile postedFile = HttpContext.Current.Request.Files["file"] ?? HttpContext.Current.Request.Files[0];

			Stream fs = postedFile.InputStream;

			var output = new byte[] { };
			var writer = new WriteExcel();

			output = writer.Write(fs).ToArray();

			var result = new HttpResponseMessage(HttpStatusCode.OK) { Content = new ByteArrayContent(output) };
			result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = "Transformed.xlsx"
			};
			return result;

		}
	}
}