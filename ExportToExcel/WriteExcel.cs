﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetLight;
using WriteToExcel.JSONReader;
using WriteToExcel.Models;

namespace WriteToExcel.ExcelWriter
{
	public class WriteExcel
	{
		private const string FilePath = @"C:\Rahul\output.json";
		private const string OutputFilePath = @"C:\Rahul\Excel\output.xlsx";

		private static readonly JsonReader Reader = new JsonReader(FilePath);

		public static void Write()
		{
			ModelOutput readJSON = Reader.ReadJson();

			SLDocument sl = new SLDocument();

			foreach (ModelResult modelResult in readJSON.ModelResults)
			{
				IEnumerable<DependentVariable> dependentVars = modelResult.DependentVariables;
				string segmentName = modelResult.Segment;
				sl.AddWorksheet(segmentName);
				int z = 3;
				int b = 0;
				int y = 3;
				foreach (DependentVariable dependentVariable in dependentVars)
				{
					IEnumerable<IndependentVariable> independentVars = dependentVariable.IndependentVariables;
					string dependentVarName = dependentVariable.Name;
					PropertyInfo[] propertyNames = typeof (IndependentVariable).GetProperties();
					
					sl.SetCellValue(2, z, dependentVarName);
					foreach (PropertyInfo propertyName in propertyNames)
					{
						if (propertyName.Name.Equals("name", StringComparison.OrdinalIgnoreCase))
						{
							continue;
						}
						sl.SetCellValue(3, z++, propertyName.Name);
					}
					b++;
					int c = 0;
					foreach (IndependentVariable independentVariable in independentVars)
					{
						c++;
						if (b > 1)
						{
							y = propertyNames.Length + 2;
							if (y > propertyNames.Length * 2)
							{
								y = propertyNames.Length + 2;
							}
						}
						else
						{
							if (y > propertyNames.Length + 1)
							{
								y = 3;
							}
						}
						string name = independentVariable.Name;
						sl.SetCellValue(c + 3, 2, name);
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.UnivariatePValue, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.TValue, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.Coefficient, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.UnivariateCoefficient, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.UnivariateStdErr, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.StdError, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.AlteredEffect, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.PValue, 3));
						sl.SetCellValue(c + 3, y++, Math.Round(independentVariable.UnivariateTValue, 3));
					}
				}


				var dbo = new SLDataBarOptions(SLConditionalFormatDataBarValues.Green)
				{
					NegativeFillColor = { Color = System.Drawing.Color.IndianRed },
					MinimumType = SLConditionalFormatAutoMinMaxValues.Automatic,
					MaximumType = SLConditionalFormatAutoMinMaxValues.Automatic,
					AxisColor = { Color = System.Drawing.Color.DodgerBlue },
					AxisPosition = DocumentFormat.OpenXml.Office2010.Excel.DataBarAxisPositionValues.Middle
				};
				var cf1 = new SLConditionalFormatting("C1", "C100");
				var cf2 = new SLConditionalFormatting("D1", "D100");
				var cf3 = new SLConditionalFormatting("E1", "E100");
				var cf4 = new SLConditionalFormatting("F1", "F100");
				var cf5 = new SLConditionalFormatting("G1", "G100");
				var cf6 = new SLConditionalFormatting("H1", "H100");
				var cf7 = new SLConditionalFormatting("I1", "I100");
				var cf8 = new SLConditionalFormatting("J1", "J100");
				var cf9 = new SLConditionalFormatting("K1", "K100");
				var cf10 = new SLConditionalFormatting("L1", "L100");
				var cf11 = new SLConditionalFormatting("M1", "M100");
				var cf12 = new SLConditionalFormatting("N1", "N100");
				var cf13 = new SLConditionalFormatting("O1", "O100");
				var cf14 = new SLConditionalFormatting("P1", "P100");
				var cf15 = new SLConditionalFormatting("Q1", "Q100");
				var cf16 = new SLConditionalFormatting("R1", "R100");
				var cf17 = new SLConditionalFormatting("S1", "S100");
				var cf18 = new SLConditionalFormatting("T1", "T100");
				cf1.SetCustomDataBar(dbo);
				cf2.SetCustomDataBar(dbo);
				cf3.SetCustomDataBar(dbo);
				cf4.SetCustomDataBar(dbo);
				cf5.SetCustomDataBar(dbo);
				cf6.SetCustomDataBar(dbo);
				cf7.SetCustomDataBar(dbo);
				cf8.SetCustomDataBar(dbo);
				cf9.SetCustomDataBar(dbo);
				cf10.SetCustomDataBar(dbo);
				cf11.SetCustomDataBar(dbo);
				cf12.SetCustomDataBar(dbo);
				cf13.SetCustomDataBar(dbo);
				cf14.SetCustomDataBar(dbo);
				cf15.SetCustomDataBar(dbo);
				cf16.SetCustomDataBar(dbo);
				cf17.SetCustomDataBar(dbo);
				cf18.SetCustomDataBar(dbo);

				sl.AddConditionalFormatting(cf1);
				sl.AddConditionalFormatting(cf2);
				sl.AddConditionalFormatting(cf3);
				sl.AddConditionalFormatting(cf4);
				sl.AddConditionalFormatting(cf5);
				sl.AddConditionalFormatting(cf6);
				sl.AddConditionalFormatting(cf7);
				sl.AddConditionalFormatting(cf8);
				sl.AddConditionalFormatting(cf9);
				sl.AddConditionalFormatting(cf10);
				sl.AddConditionalFormatting(cf11);
				sl.AddConditionalFormatting(cf12);
				sl.AddConditionalFormatting(cf13);
				sl.AddConditionalFormatting(cf14);
				sl.AddConditionalFormatting(cf15);
				sl.AddConditionalFormatting(cf16);
				sl.AddConditionalFormatting(cf17);
				sl.AddConditionalFormatting(cf18);

			}
			sl.DeleteWorksheet("Sheet1");
			sl.SaveAs(OutputFilePath);

			Console.WriteLine("End of program");
			Console.ReadLine();
		}

		
	}
}
	