﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WriteToExcel.Models
{
	public class Diagnostic
	{
		public string Name { get; set; }

		public double Value { get; set; }
	}
}
