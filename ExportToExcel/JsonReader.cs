﻿using System.IO;
using Newtonsoft.Json;
using WriteToExcel.Models;

namespace WriteToExcel.JSONReader
{
	public class JsonReader
	{
		private readonly string _filePath;
		
		public JsonReader(string filePath)
		{
			_filePath = filePath;
		}

		public ModelOutput ReadJson()
		{
			using (StreamReader r = new StreamReader(_filePath))
			{
				string json = r.ReadToEnd();

				return JsonConvert.DeserializeObject<ModelOutput>(json);
			}
		}
	}
}
