﻿using System.Collections.Generic;

namespace ExportToExcel.Models
{
	public class ModelResult
	{
		public string Segment { get; set; }
		public IEnumerable<DependentVariable> DependentVariables { get; set; }
	}
}
