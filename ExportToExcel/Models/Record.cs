﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExportToExcel.Models
{
	public class Record
	{
		public string FName { get; set; }
		public string LName { get; set; }
		public string Address { get; set; }
	}
}