﻿using System.IO;
using ExportToExcel.Models;
using Newtonsoft.Json;

namespace ExportToExcel.ExcelWriter
{
	public class JsonReader
	{
		private readonly string _filePath;
		
		public JsonReader(string filePath)
		{
			_filePath = filePath;
		}

		public ModelOutput ReadJson(Stream s)
		{
			using (var r = new StreamReader(s))
			{
				string json = r.ReadToEnd();

				return JsonConvert.DeserializeObject<ModelOutput>(json);
			}
		}
	}
}
