﻿using System.Collections.Generic;

namespace WriteToExcel.Models
{
	public class ModelResult
	{
		public string Segment { get; set; }
		public IEnumerable<DependentVariable> DependentVariables { get; set; }
	}
}
