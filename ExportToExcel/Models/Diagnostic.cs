﻿
namespace ExportToExcel.Models
{
	public class Diagnostic
	{
		public string Name { get; set; }

		public double Value { get; set; }
	}
}
