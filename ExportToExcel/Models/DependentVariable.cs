﻿using System.Collections.Generic;

namespace ExportToExcel.Models
{
	public class DependentVariable
	{
		public string Name { get; set; }
		public IEnumerable<IndependentVariable> IndependentVariables { get; set; }
		public IEnumerable<Diagnostic> Diagnostics { get; set; }
	}
}
