﻿
namespace WriteToExcel.Models
{
	public class IndependentVariable
	{
		public string Name { get; set; }

		public double UnivariatePValue { get; set; }

		public double TValue { get; set; }

		public double Coefficient { get; set; }

		public double UnivariateCoefficient { get; set; }

		public double UnivariateStdErr { get; set; }

		public double StdError { get; set; }

		public double AlteredEffect { get; set; }

		public double PValue { get; set; }

		public double UnivariateTValue { get; set; }
	}
}
